<?php get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php
		/* Start the Loop */
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );
?>
<div id="fb-root"></div>
<div class="wrapper bright-bg centered" style="margin-top: 16px">
<?php comments_template(); ?>
</div>
<?php
	endwhile; // End of the loop.
?>
	</main><!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>