<?php get_header(); ?>
    <div class="wrapper book-bg">
        <div class="content">
            <div style="max-width: 60%">
                <div style="font-size: 3em; line-height: initial;">Sarian</div>
                <div style="font-size: 1.4em; line-height: initial;">The Young Savior of a Falling Kingdom</div>
                <p>She thinks herself impotent, a mere child who cannot change a thing. But she is proven wrong, when she receives a strange offer. All of a sudden, she is the only one who can bring back peace. All she needs to do is leave her life behind. Sometimes, you need to make sacrifices for a greater good.</p>
            </div>
        </div>
    </div>
    <div class="wrapper bright-bg">
        <div class="content">
            <div class="about-me">
                <div style="width: 260px; height: inherit; vertical-align: middle;"><img src="<?php ferry_theme_images(); ?>/ferry.jpg" alt="H. Ferry" class="my-photo" /></div>
                <div style="max-width: 620px; height: inherit; vertical-align: middle;">
                    <h2>About Me</h2>
					<?php
					if ( have_posts() ) :
						/* Start the Loop */
						while ( have_posts() ) : the_post();
							the_content();
						endwhile;
					else : echo "Sorry, no posts matched your criteria.";
					endif;
				?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>