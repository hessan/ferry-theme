<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
			if ( have_posts() ) :

				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_format() );

				endwhile;

				the_posts_pagination( array(
					'prev_text' => '<span class="screen-reader-text">Previous</span>',
					'next_text' => '<span class="screen-reader-text">Next</span>',
					'before_page_number' => '<span class="meta-nav screen-reader-text">Page </span>',
				) );

			else :
				echo "Sorry, no posts matched your criteria.";

			endif;
			?>

		</main>
	</div>

<?php get_footer(); ?>