<?php
	$pn = get_option("ferry_contact_phone", "");
	$em = get_option("ferry_contact_email", "");
	$tw = get_option("ferry_contact_twitter", "");
	$fb = get_option("ferry_contact_facebook", "");
	$li = get_option("ferry_contact_linkedin", "");
	$sk = get_option("ferry_contact_skype", "");
	$sw = get_option("ferry_contact_smashwords", "");
	$az = get_option("ferry_contact_amazon", "");
?>
<div class="wrapper dark-bg footer centered">
    <div class="copyright">Copyright 2017. All rights reserved.</div>
	<div id="contact" style="display: inline-block;">
		<?php if($pn != "") : ?><a href="tel:<?php echo $pn; ?>" itemprop="telephone" title="Phone" class="social phone" target="_blank"></a><?php endif; ?>
		<?php if($em != "") : ?><a href="mailto:<?php echo $em; ?>" itemprop="email" title="Email" class="social email" target="_blank"></a><?php endif; ?>
		<?php if($tw != "") : ?><a href="https://twitter.com/<?php echo $tw; ?>" title="Twitter" class="social twitter" target="_blank"></a><?php endif; ?>
		<?php if($fb != "") : ?><a href="https://facebook.com/<?php echo $fb; ?>" title="Facebook" class="social facebook" target="_blank"></a><?php endif; ?>
		<?php if($li != "") : ?><a href="https://www.linkedin.com/in/<?php echo $li; ?>" title="LinkedIn" class="social linkedin" target="_blank"></a><?php endif; ?>
		<?php if($sk != "") : ?><a href="skype:<?php echo $sk; ?>?chat" title="Skype" class="social skype"></a><?php endif; ?>
		<?php if($sw != "") : ?><a href="https://www.smashwords.com/profile/view/<?php echo $sw; ?>" title="Smashwords" class="social smashwords" target="_blank"></a><?php endif; ?>
		<?php if($az != "") : ?><a href="https://www.amazon.com/<?php echo $az; ?>" title="Amazon" class="social amazon" target="_blank"></a><?php endif; ?>
		<a href="/feed/" title="RSS" class="social rss" target="_blank"></a>
	</div>
</div>
<script>
    (function () {

        // Create mobile element
        var mobile = document.createElement('div');
        mobile.className = 'nav-mobile';
        document.querySelector('.nav').appendChild(mobile);

        // hasClass
        function hasClass(elem, className) {
            return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
        }

        // toggleClass
        function toggleClass(elem, className) {
            var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(' ' + className + ' ') >= 0) {
                    newClass = newClass.replace(' ' + className + ' ', ' ');
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            } else {
                elem.className += ' ' + className;
            }
        }

        // Mobile nav function
        var mobileNav = document.querySelector('.nav-mobile');
        var toggle = document.querySelector('.menu');
        mobileNav.onclick = function () {
            toggleClass(this, 'nav-mobile-open');
            toggleClass(toggle, 'nav-active');
        };
    })();
</script>
</html>