<?php get_header(); ?>
<div class="wrapper">
<div class="centered world">
	<?php
	while ( have_posts() ) {
		the_post();
		the_content();
	}
	?>
</div>
</div>
<?php get_footer(); ?>