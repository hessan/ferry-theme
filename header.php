<!doctype html>
<html lang="en">
<head>
<title>Home - H. Ferry</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<style>
html {
    color: rgb(213, 255, 251);
    font-family: 'Roboto', sans-serif;
	line-height: 150%;
    font-size: 1.4em;
    margin: 0;
    padding: 0;
    background: url('<?php ferry_theme_images(); ?>background.jpg') no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
body { margin: 0; }
figure {text-align:center;}
figure img, figure>div {border:4px solid white;margin:24px auto 24px auto;box-shadow:0px 0px 10px 4px rgba(119, 119, 119, 0.75);-moz-box-shadow:0px 0px 10px 4px rgba(119, 119, 119, 0.75);-webkit-box-shadow:0px 0px 10px 4px rgba(119, 119, 119, 0.75);}
img.large, figure img {max-width:100%;}
a {
	color: #ddeedd;
	text-decoration: none;
}
a:hover {
	color: #ffffff;
	text-decoration: underline;
}
.bright-bg a {
	color: #5555ff
}
.bright-bg a:hover {
	color: #7777ff
}
nav ul, nav ol { list-style: none; }
.wrapper { margin: 0 auto; position: relative; width: 100%; }
.content, article {
    text-align: left;
    margin: 0px auto 0px auto;
    padding: 10px 10px 10px 10px;
    max-width: 960px;
}
article {
	margin-top: 24px;
}
article.type-post {
	border: 8px solid rgba(255, 255, 255, 0.9);
	background-color: white;
	box-shadow:0px 0px 10px 4px rgba(0, 0, 0, 0.5);
	-moz-box-shadow:0px 0px 10px 4px rgba(0, 0, 0, 0.5);
	-webkit-box-shadow:0px 0px 10px 4px rgba(0, 0, 0, 0.5);
	color: black;
}
.type-post a {
	color: #0c3640;
}
.type-post a:hover {
	color: #0f4654;
}
.entry-meta {
	font-size: small;
	text-align: right;
}
.entry-content {
	margin-top: 0;
}
blockquote {
	color: white;
	background: transparent url(<?php ferry_theme_images(); ?>quote-bg.jpg) no-repeat center center;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
	margin: 0;
	padding: 4px 32px 4px 32px;
    -webkit-margin-before: 0;
    -webkit-margin-after: 0;
    -webkit-margin-start: 0;
    -webkit-margin-end: 0;
	font-size: 140%;
	text-align: center;
	line-height: 130%;
	border: 6px solid white;
	box-shadow: 0px 0px 10px 4px rgba(119, 119, 119, 0.5);
	-moz-box-shadow:0px 0px 10px 4px rgba(119, 119, 119, 0.5);
	-webkit-box-shadow:0px 0px 10px 4px rgba(119, 119, 119, 0.5);
}
h2 {
	margin: 0;
	padding: 0;
}
.entry-title:first-letter {
	font-size: larger;
}
.dark-bg { background-color: rgba(0, 0, 0, 0.6); }
.bright-bg { background-color: rgba(255, 255, 255, 0.8); color: black; }
.centered { text-align: center; }
.nav {
    margin: 0px auto 0px auto;
    display: inline-block;
    font-size: 0.8em;
}
.menu-item {
    float: left;
    *display: inline;
    zoom: 1;
}
.menu-item a {
    text-decoration: none;
    display: block;
    padding: 15px 20px;
    color: rgba(255, 255, 255, 0.75);
    margin-bottom: 1px;
}
.menu-item a:first-letter {
    font-size: larger;
}
.menu-item a:hover {
    color: white;
    border-bottom: 2px solid white;
    margin-bottom: 0px;
}
.current-menu-item a {
    color: white;
    border-bottom: 1px solid white;
}

/* Mobile Navigation */
.nav-mobile {
    display: none; /* Hide from browsers that don't support media queries */
    cursor: pointer;
    position: absolute;
    top: 0;
    right: 0;
    background: transparent url(<?php ferry_theme_images(); ?>nav.svg) no-repeat center center;
    height: 40px;
    width: 40px;
}

@media only screen and (min-width: 320px) and (max-width: 768px) {
    .nav-mobile {
        display: block;
    }
    .nav {
        width: 100%;
        padding: 40px 0 0;
    }
    .menu {
        display: none;
    }
    .menu-item {
        width: 100%;
        float: none;
    }
    .menu-item a, .menu-item a:hover {
        border: 0;
		margin-bottom: 0;
    }
    .nav-active {
        display: block;
    }
}
.about-me {
    width: 100%;
}
.about-me div {
    display: inline-block;
    margin: 16px;
}
.my-photo {
    border-radius: 125px;
    border: 4px solid rgba(255, 255, 255, 0.5);
    text-align: center;
}
.book-bg {
    background: url('<?php ferry_theme_images(); ?>books/sarian-landscape.jpg') no-repeat bottom right;
    padding-top: 48px;
	padding-bottom: 48px;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    border-top: 4px solid black;
    border-bottom: 4px solid black;
}
.book-listing, .world {
	padding-top: 24px;
	font-size: medium;
}
.book-cover {
	border: 4px solid rgba(255, 255, 255, 0.9);
	box-shadow:0px 0px 10px 4px rgba(0, 0, 0, 0.5);
	-moz-box-shadow:0px 0px 10px 4px rgba(0, 0, 0, 0.5);
	-webkit-box-shadow:0px 0px 10px 4px rgba(0, 0, 0, 0.5);
	width: 200px;
}
.footer {
	font-size: medium;
	margin-top: 60px;
}
.footer div {
	padding: 24px;
}
.copyright {display: inline-block; padding: 8px; color: rgba(255, 255, 255, 0.75); font-size: small;}
.social {background:url(<?php ferry_theme_images(); ?>social-icons.png) no-repeat top left;width:32px;height:32px;display:inline-block;}
.phone{background-position:0 0;}
.email{background-position:-32px 0;}
.twitter{background-position:-64px 0;}
.facebook{background-position:-96px 0;}
.linkedin{background-position:-128px 0;}
.gplus{background-position:-160px 0;}
.skype{background-position:-192px 0;}
.smashwords{background-position:-224px 0;}
.amazon{background-position:-256px 0;}
.rss{background-position:-288px 0;}
.fb-comments {margin: 0 auto 0 auto;}
.social-share iframe {
	vertical-align: middle;
}
.world .excerpt {
	margin: 12px auto 12px auto;
	max-width: 400px;
	background-color: rgba(0, 0, 0, 0.7);
	padding: 24px;
	border: 1px solid black;
}
.world .listing {
	text-align: left;
	margin: 0px auto 12px auto;
	max-width: 400px;
	background-color: rgba(0, 50, 20, 0.6);
	padding: 24px;
	border: 1px solid black;
}
.world .listing h3 {
	margin: 0px;
	heading: 0px;
}
.world .header {
	margin: 32px auto 0px auto;
	background-image: url(<?php ferry_theme_images(); ?>headerbar.png);
	background-position: center center;
	background-repeat: no-repeat;
	width: 500px;
	height: 107px;
}

.world .header img {
	margin-top: 12px;
	width: 72px;
	height: 72px;
	border: 4px solid black;
    border-radius: 36px;
	float: left;
}
.world .header h1, .world .header h2 {
	margin-top: 12px;
	margin-left: 32px;
	padding-top: 22px;
	font-size: 24px;
	min-width: 320px;
	text-align: left;
	height: 50px;
	float: left;
}
.wpcf7 { text-align: center; }
.wpcf7-form-control-wrap div { margin: 0 auto; }
.wpcf7 label {
	margin: 0 auto;
	text-align: left;
    background-color: rgba(0, 0, 0, 0.6);
	border: 1px solid black;
	padding: 8px;
	display: block;
	border-radius: 8px;
	max-width: 400px;
	font-size: 1rem;
}
.wpcf7-response-output {
	margin: 0 auto;
	border-radius: 8px;
	max-width: 400px;
	display: inline-block;
    background-color: rgba(0, 0, 0, 0.5);
}
.wpcf7 input[type="text"],
.wpcf7 input[type="email"],
.wpcf7 textarea {
    font-family: 'Roboto', sans-serif;
	background-color: transparent;
	border: 0px;
	font-size: 1em;
	color: white;
	width: 100%;
	border-bottom: 1px solid rgba(255, 255, 255, 0.8);
}
.wpcf7 input[type="submit"]
{
    background-color: rgba(0, 100, 50, 0.8);
	border: 2px solid rgba(0, 200, 100, 0.8);
	border-radius: 4px;
	padding: 8px;
    color: white;
	font-size: 1em;
	width: 200px;
}
.wpcf7 span[role="alert"] {
	font-size: 0.75em;
	padding: 4px;
	background-color: rgba(200, 50, 50, 0.5);
	border-radius: 0px 0px 6px 6px;
	color: #aa0000;
}
</style>
<script src='https://www.google.com/recaptcha/api.js'></script>
<?php wp_head(); ?>
</head>
<body>
<?php include_once("inc/analytics.php") ?>
<div class="wrapper dark-bg centered">
    <nav class="nav">
		<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
    </nav>
</div>
