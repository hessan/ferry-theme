	<div class="wrap">
	<h2>Configure Ferry Theme</h2>
	<form method="POST" action="">
	<input type="hidden" name="update_settings" value="HSS1232" />
	<p><input type="submit" value="Update Settings" class="button-primary"/></p>
	<h3 class="title">Basic Information</h3>
	<table class="form-table">
		<tr valign="top">
			<th><label for="contact_name">Name</label></th>
			<td><input name="contact_name" type="text" id="contact_name" value="<?php echo get_option("ferry_contact_name", "Your Name"); ?>" class="regular-text ltr" /></td>
		</tr>
		<tr valign="top">
			<th><label for="theme_birthday">Birthday</label></th>
			<td><input type="date" id="theme_birthday" name="theme_birthday" value="<?php echo get_option("ferry_theme_birthday", ""); ?>" class="example-datepicker" /></td>
		</tr>
	</table>
	<h3 class="title">Contact Information</h3>
	<table class="form-table">
		<tr valign="top">
			<th><label for="contact_email">Email Address</label></th>
			<td><input name="contact_email" type="text" id="contact_email" value="<?php echo get_option("ferry_contact_email", ""); ?>" class="regular-text ltr" /></td>
		</tr>
		<tr valign="top">
			<th><label for="contact_phone">Phone Number</label></th>
			<td><input name="contact_phone" type="text" id="contact_phone" value="<?php echo get_option("ferry_contact_phone", ""); ?>" class="regular-text ltr" /></td>
		</tr>
		<tr valign="top">
			<th><label for="contact_skype">Skype</label></th>
			<td><input name="contact_skype" type="text" id="contact_skype" value="<?php echo get_option("ferry_contact_skype", ""); ?>" class="regular-text ltr" /></td>
		</tr>
		<tr valign="top">
			<th><label for="contact_facebook">Facebook</label></th>
			<td><input name="contact_facebook" type="text" id="contact_facebook" value="<?php echo get_option("ferry_contact_facebook", ""); ?>" class="regular-text ltr" /></td>
		</tr>
		<tr valign="top">
			<th><label for="contact_linkedin">Linkedin Profile Id</label></th>
			<td><input name="contact_linkedin" type="text" id="contact_linkedin" value="<?php echo get_option("ferry_contact_linkedin", ""); ?>" class="regular-text ltr" /></td>
		</tr>
		<tr valign="top">
			<th><label for="contact_twitter">Twitter</label></th>
			<td><input name="contact_twitter" type="text" id="contact_twitter" value="<?php echo get_option("ferry_contact_twitter", ""); ?>" class="regular-text ltr" /></td>
		</tr>
		<tr valign="top">
			<th><label for="contact_smashwords">Smashwords</label></th>
			<td><input name="contact_smashwords" type="text" id="contact_smashwords" value="<?php echo get_option("ferry_contact_smashwords", ""); ?>" class="regular-text ltr" /></td>
		</tr>
		<tr valign="top">
			<th><label for="contact_github">Amazon</label></th>
			<td><input name="contact_amazon" type="text" id="contact_amazon" value="<?php echo get_option("ferry_contact_amazon", ""); ?>" class="regular-text ltr" /></td>
		</tr>
	</table>
	<p><input type="submit" value="Update Settings" class="button-primary"/></p>
	</form>
	</div>
