<?php

if ( ! function_exists( 'ferry_setup' ) ) :
function ferry_setup() {
	add_theme_support( 'title-tag' );
	
	register_nav_menu('main-menu', __( 'Main Menu' ));
	
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	));
}
endif;

if ( ! function_exists( 'ferry_theme_settings' ) ) :
function ferry_theme_settings() {
	if (!current_user_can('manage_options')) {  
		wp_die('You do not have sufficient permissions to access this page.');  
	}

	if (isset($_POST["update_settings"])) {
		update_option("ferry_contact_name", $_POST["contact_name"]);
		update_option("ferry_contact_email", $_POST["contact_email"]);
		update_option("ferry_contact_phone", $_POST["contact_phone"]);
		update_option("ferry_contact_linkedin", $_POST["contact_linkedin"]);
		update_option("ferry_contact_facebook", $_POST["contact_facebook"]);
		update_option("ferry_contact_twitter", $_POST["contact_twitter"]);
		update_option("ferry_contact_smashwords", $_POST["contact_smashwords"]);
		update_option("ferry_contact_amazon", $_POST["contact_amazon"]);
		update_option("ferry_contact_skype", $_POST["contact_skype"]);
		update_option("ferry_theme_birthday", $_POST["theme_birthday"]);
		echo '<div id="message" class="updated">Settings saved.</div>';
	}

	include 'inc/settings.php';
}
endif;

if ( ! function_exists( 'ferry_setup_admin_menus' ) ) :
function ferry_setup_admin_menus() {  
    add_submenu_page('themes.php',
		'Configure Ferry Theme', 'Ferry Theme', 'manage_options',
		'ferry-theme-settings', 'ferry_theme_settings');
}
endif;

if ( ! function_exists( 'ferry_theme_images' ) ) :
function ferry_theme_images() {
	echo get_bloginfo('stylesheet_directory') . '/images/';
}
endif;

if ( ! function_exists( 'ferry_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function ferry_posted_on() {

	// Get the author name; wrap it in a link.
	$byline = sprintf(
		/* translators: %s: post author */
		'by %s',
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . get_the_author() . '</a></span>'
	);

	// Finally, let's write all of this to the page.
	echo '<span class="posted-on">' . ferry_time_link() . '</span><span class="byline"> ' . $byline . '</span>';
}
endif;

if ( ! function_exists( 'ferry_time_link' ) ) :
/**
 * Gets a nicely formatted string for the published date.
 */
function ferry_time_link() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time> (updated <time class="updated" datetime="%3$s">%4$s</time>)';
	}

	$time_string = sprintf( $time_string,
		get_the_date( DATE_W3C ),
		get_the_date(),
		get_the_modified_date( DATE_W3C ),
		get_the_modified_date()
	);

	// Wrap the time string in a link, and preface it with 'Posted on'.
	return sprintf(
		/* translators: %s: post date */
		'<span class="screen-reader-text">Posted on</span> %s',
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);
}

function ferry_listing_shortcode( $atts, $content = null ) {
    $a = shortcode_atts( array('tag' => '', 'title' => 'Related Posts'), $atts );
	$ret = '<div class="listing"><h3>' . $a['title'] . '</h3><ul>';

	$the_query = new WP_Query( 'tag=' . $a['tag'] . '&order=ASC' );

	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$ret .= '<li><a href="' . esc_url( get_permalink() ) . '" target="_blank">' . get_the_title() . '</a></li>';
		}
	}
	wp_reset_postdata();
	return $ret . '</ul></div>';
}

function ferry_character_badge_shortcode( $atts, $content = null ) {
    $a = shortcode_atts( array('name' => 'Character', 'excerpt' => null), $atts );
	$ret = '<div class="header">' . do_shortcode($content) . '<h2>' . $a['name'] . '</h2></div>';
	if ($a['excerpt'] != null) {
		$ret .= '<div class="excerpt">“' . $a['excerpt'] . '”</div>';
	}
	return $ret;
}

endif;

add_shortcode( 'character', 'ferry_character_badge_shortcode' );
add_shortcode( 'listing', 'ferry_listing_shortcode' );
add_action( 'after_setup_theme', 'ferry_setup' );
add_action( 'admin_menu', 'ferry_setup_admin_menus' );
?>