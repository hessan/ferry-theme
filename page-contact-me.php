<?php get_header(); ?>
<div class="wrapper">
<div class="centered world">
	<div class="header"><img src="http://www.ferryfiction.com/wp-content/themes/ferry/images/ferry.jpg"><h1>Drop me a line!</h1></div>
	<?php
	while ( have_posts() ) {
		the_post();
		the_content();
	}
	?>
</div>
</div>
<?php get_footer(); ?>